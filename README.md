# Bachelor Thesis
My bachelor thesis utilizes distributed computing, large data processing, data feature weight approximation and similarity analysis. The result is a system, which analyzes a database of molecules and predicts active compounds (e.g. medication).

Implemented using Python, C++ and Bash.  
The thesis was awarded with the highest possible grade.

Overview:  
Virtuální screening (VS) is a method for in-silico identification of bioactive compounds where the input is a database of small molecules and the output is ranking of the database based on the probability of the input molecules to bind to given macromolecular target. Often used approach to VS is ligand-based VS (LBVS). In LBVS, the database is prioritized based on the similarity of the compounds being similar to one or more compounds for which their binding affinity to the target is already known. The key determinant of VS performance is the computational representation of a molecule and how molecular similarity is defined. Commonly used molecular representations describe molecule as a list of its constituent fragments or physico-chemical properties, or fragments properties. Molecular similarity is then defined as similarity of the lists. However, all fragments get the same weight. The goal of the thesis is to evaluate possibilities of weighting fragments or fragments properties and possibilities of molecular similarity parametrization. In the first phase of the project, the student will propose similarity measure defining similarity of molecules as similarity of properties of their constituting fragments. In the next phase, the student will evaluate the possibility of using simulated annealing to optimize individual weights.

# WeFrag

The tutorial to run WeFrag can be found in User Documentation section in Thesis.pdf.
