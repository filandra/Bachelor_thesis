# !/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

"""
Just a small script to merge .sdf files

-te - test.sdf
-tr - train.sdf
-va - validation.sdf
-o - output
"""


def _read_configuration():
	parser = argparse.ArgumentParser(
		description='.sdf files merger')
	parser.add_argument('-te', type=str, dest='test',
						help='Input test .sdf',
						required=False)
	parser.add_argument('-tr', type=str, dest='train',
						help='Input train .sdf',
						required=False)
	parser.add_argument('-va', type=str, dest='validation',
						help='Input validation .sdf', required=False)
	parser.add_argument('-o', type=str, dest='output',
						help='Output file', required=True)

	return vars(parser.parse_args())


def _main():
	configuration = _read_configuration();

	# Does not fail, if the file does not exist
	with open(configuration["output"], "w+") as o:
		if "test" in configuration:
			try:
				with open(configuration["test"]) as f:
					for line in f:
						o.write(line)
			except:
				pass
		if "train" in configuration:
			try:
				with open(configuration["train"]) as f:
					for line in f:
						o.write(line)
			except:
				pass

		if "validation" in configuration:
			try:
				with open(configuration["validation"]) as f:
					for line in f:
						o.write(line)
			except:
				pass


if __name__ == '__main__':
	_main()
