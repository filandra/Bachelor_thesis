#include <iostream>
#include <cmath>
#include <fstream>
#include <sstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <memory>
#include "val.h"
#include "Comparer.h"
#include "Euclidean_NFrag.h"
#include "Manhattan_NFrag.h"
#include "Simple_NFrag.h"
#include "Optimizer.h"
#include "Simulated_Annealing.h"
#include <ctime>

// Just to sort in descending order
struct Sort_Comparator {
    bool operator()(const val &left, const val &right) {
        return left.value > right.value;
    }
};

// reads molecules from input
int fill(std::vector<int> *v, int num, std::ifstream &infile) {

    std::string line;
    std::string token;
    int max_frag_num = 0;
    int frag_num = 0;

    // For each molecule save the fragments into vector
    for (int i = 0; i < num; i += 1) {
        std::getline(infile, line);
        std::istringstream buf(line);
        while (buf >> token) {
            v[i].push_back(std::stoi(token));
            frag_num++;
        }
        if (frag_num > max_frag_num) {
            max_frag_num = frag_num;
        }
    }

    return max_frag_num;
}


double rand_double(double fMin, double fMax) {
    double f = (double) rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

// Calculates the receiver operating characteristics
void calc_ROC(std::vector<val> &best_compared, double *TPR, double *TNR) {

    int num_mol = best_compared.size();
    int num_actives = 0;
    int num_inactives = 0;

    for (int i = 0; i < num_mol; ++i) {
        //true==active
        if (best_compared[i].declared == true) {
            num_actives++;
        } else {
            num_inactives++;
        }
        TPR[i] = num_actives;
        TNR[i] = num_inactives;
    }

    //Normalize
    if (num_actives > 0) {
        for (int i = 0; i < num_mol; ++i) {
            TPR[i] = TPR[i] / num_actives;
        }
    }

    if (num_inactives > 0) {
        for (int i = 0; i < num_mol; ++i) {
            TNR[i] = TNR[i] / num_inactives;
        }
    }

}

// Calculates area under the curve (ROC curve)
double calc_AUC(int num_mol, double *TPR, double *TNR) {
    double AUC = 0;

    for (int i = 0; i < num_mol - 1; ++i) {
        AUC += (TNR[i + 1] - TNR[i]) * (TPR[i + 1] + TPR[i]);
    }

    return 0.5 * AUC;
}


double calc_ROC_AUC(std::vector<val> &best_compared, double *TPR, double *TNR) {
    //Zesortovat od největšího po nejmenší

    Sort_Comparator sort_comparator;
    std::sort(best_compared.begin(), best_compared.end(), sort_comparator);

    calc_ROC(best_compared, TPR, TNR);

    double AUC = calc_AUC(best_compared.size(), TPR, TNR);

    return AUC;

}

// One iteration of screening
double
iteration(std::vector<int> *first_set, std::vector<int> *second_set, std::vector<int> *third_set, int first_set_num,
          int second_set_num, int third_set_num, val **cache, double **descriptors, int desc_len,
          std::vector<val> best_compared, double method, double *weights, Node *linked_list, double *TPR, double *TNR,
          Comparer *comparer, int frag_num) {

    if (first_set_num != 0 && second_set_num != 0) {
        comparer->compare_sets(first_set, first_set_num, second_set, second_set_num, cache, descriptors, desc_len,
                               best_compared, method, true, weights, linked_list, "normal");
    }
    if (first_set_num != 0 && third_set_num != 0) {
        comparer->compare_sets(first_set, first_set_num, third_set, third_set_num, cache, descriptors, desc_len,
                               best_compared, method, false, weights, linked_list, "normal");
    }
// Convert distance to similarity
    for (int j = 0; j < best_compared.size(); ++j) {
        best_compared[j].value = 1 - best_compared[j].value;
    }

    double AUC = calc_ROC_AUC(best_compared, TPR, TNR);

// Clean-up
    best_compared.clear();
    for (int i = 0; i < frag_num; ++i) {
        for (int j = 0; j < frag_num; ++j) {
            cache[i][j].declared = false;
            cache[i][j].value = 0;
        }
    }
    return AUC;
}

// Generate weights
void phase_1(char *argv[], double *weights) {
    double starting_val = 1.0;

    std::string line;
    std::ifstream infile_1(argv[1]);
    std::ifstream infile_2(argv[2]);
    // Method
    line = argv[3];
    std::string annealing_type = argv[4];
    double T = std::stod(argv[5]);
    double orig_T = std::stod(argv[5]);
    double dist;
    int parts = std::stoi(argv[7]);
    double cooling = std::stod(argv[8]);
    std::string phase = argv[9];
    std::ifstream weights_file(argv[10]);

    Comparer *comparer;
    int method = 0;
    Optimizer *optimizer = new Simulated_Annealing();

    if (line == ("simple")) {
        comparer = new Simple_NFrag();
        method = -1;
    } else {
        if (line[0] == 's') {
            std::string number = line.substr(6, line.size() - 1);
            method = std::stoi(number);
            comparer = new Simple_NFrag();
        }
    }

    if (line == ("euclidean")) {
        comparer = new Euclidean_NFrag();
        method = -1;
    } else {
        if (line[0] == 'e') {
            std::string number = line.substr(9, line.size() - 1);
            method = std::stoi(number);
            comparer = new Euclidean_NFrag();
        }
    }

    if (line == ("manhattan")) {
        comparer = new Manhattan_NFrag();
        method = -1;
    } else {
        if (line[0] == 'm') {
            std::string number = line.substr(9, line.size() - 1);
            method = std::stoi(number);
            comparer = new Manhattan_NFrag();
        }
    }

    std::vector<val> best_compared;
    int list_size = 0;
    std::vector<int> max_frag_nums;
    val **cache;
    double **descriptors;
    std::string header;

    std::getline(infile_1, line);
    int desc_len = std::stoi(line);

    std::getline(infile_1, line);
    int frag_num = std::stoi(line);

    std::getline(infile_1, line);
    int active_num_train = std::stoi(line);

    std::getline(infile_1, line);
    int active_num_validation = std::stoi(line);

    std::getline(infile_1, line);
    int active_num_test = std::stoi(line);

    std::getline(infile_1, line);
    int inactive_num_train = std::stoi(line);

    std::getline(infile_1, line);
    int inactive_num_validation = std::stoi(line);

    std::getline(infile_1, line);
    int inactive_num_test = std::stoi(line);

    dist = ((starting_val / desc_len) * std::stod(argv[6])) / 100;

    // Distribute known active molecules for training
    int active_total = active_num_train + active_num_validation;
    active_num_train = floor(float(active_total) / 4);
    active_num_validation = active_total - active_num_train;

    // Merge all known inactive
    inactive_num_train += inactive_num_validation;

    // Array if vectors
    std::vector<int> *active_train = new std::vector<int>[active_num_train];
    std::vector<int> *inactive_train = new std::vector<int>[inactive_num_train];
    std::vector<int> *active_validation = new std::vector<int>[active_num_validation];
    std::vector<int> *inactive_validation = new std::vector<int>[inactive_num_validation];
    std::vector<int> *active_test = new std::vector<int>[active_num_test];
    std::vector<int> *inactive_test = new std::vector<int>[inactive_num_test];

    cache = new val *[frag_num];
    for (int i = 0; i < frag_num; i++)
        cache[i] = new val[frag_num];

    // Clear cache just to be sure
    for (int i = 0; i < frag_num; ++i) {
        for (int j = 0; j < frag_num; ++j) {
            cache[i][j].value = 0;
            cache[i][j].declared = false;
        }
    }

    //float descriptors[frag_num][desc_len];
    descriptors = new double *[frag_num];
    for (int i = 0; i < frag_num; i++)
        descriptors[i] = new double[desc_len];

    // Read the descriptors
    int desc = 0;
    std::string token;
    for (int i = 0; i < frag_num; i += 1) {
        std::getline(infile_1, line);
        std::istringstream buf(line);
        while (buf >> token) {
            descriptors[i][desc] = std::stof(token);
            desc++;
        }
        desc = 0;
    }

    max_frag_nums.clear();
    max_frag_nums.push_back(fill(active_train, active_num_train, infile_1));
    max_frag_nums.push_back(fill(active_validation, active_num_validation, infile_1));
    // Just to skip the test molecules
    fill(active_test, active_num_test, infile_1);
    max_frag_nums.push_back(fill(inactive_train, inactive_num_train, infile_1));
    // inactive_validation is merged with inactive_train
    //max_frag_nums.push_back(fill(inactive_validation, inactive_num_validation, infile_1));
    // Just to skip the test molecules
    fill(inactive_test, inactive_num_test, infile_1);

    std::getline(infile_1, line);
    header = line;


    delete[] active_test;
    delete[] inactive_test;

    list_size = *std::max_element(max_frag_nums.begin(), max_frag_nums.end());
    list_size++;

    best_compared.reserve(active_num_train * active_num_validation + active_num_train * inactive_num_train);
    double *TPR = new double[active_num_train * active_num_validation + active_num_train * inactive_num_train];
    double *TNR = new double[active_num_train * active_num_validation + active_num_train * inactive_num_train];
    double AUC = 0;
    double AUC_const = 0;
    double AUC_random = 0;
    Node *linked_list = new Node[list_size];

    if (parts > desc_len) {
        parts = desc_len;
    }

    //Random weights:
    for (int k = 0; k < desc_len; ++k) {
        weights[k] = rand_double(0, starting_val / desc_len);
    }
    AUC_random = iteration(active_train, active_validation, inactive_train, active_num_train, active_num_validation,
                           inactive_num_train, cache, descriptors, desc_len, best_compared, method, weights,
                           linked_list, TPR, TNR, comparer, frag_num);

    if (annealing_type == "none") {

        for (int k = 0; k < desc_len; ++k) {
            weights[k] = starting_val / desc_len / 2;
        }

        AUC = iteration(active_train, active_validation, inactive_train, active_num_train, active_num_validation,
                        inactive_num_train, cache, descriptors, desc_len, best_compared, method, weights, linked_list,
                        TPR, TNR, comparer, frag_num);

        AUC_const = AUC;

    }

    if (annealing_type == "rand_start" || annealing_type == "const_start") {

        double *old_weights = new double[desc_len];
        double old_AUC = 0;

        if (annealing_type == "const_start") {
            for (int k = 0; k < desc_len; ++k) {
                weights[k] = starting_val / desc_len / 2;
                old_weights[k] = starting_val / desc_len / 2;
            }
        }

        if (annealing_type == "rand_start") {
            for (int k = 0; k < desc_len; ++k) {
                weights[k] = rand_double(0, starting_val / desc_len);
                old_weights[k] = weights[k];
            }
        }

        AUC_const = iteration(active_train, active_validation, inactive_train, active_num_train, active_num_validation,
                              inactive_num_train, cache, descriptors, desc_len, best_compared, method, weights,
                              linked_list, TPR, TNR, comparer, frag_num);

        // Simulated annealing
        while (T > 1) {

            AUC = iteration(active_train, active_validation, inactive_train, active_num_train, active_num_validation,
                            inactive_num_train, cache, descriptors, desc_len, best_compared, method, weights,
                            linked_list, TPR, TNR, comparer, frag_num);

            optimizer->calc_weights(old_weights, weights, old_AUC, AUC, T, dist, parts, desc_len, cooling, orig_T,
                                    starting_val);
        }

        for (int l = 0; l < desc_len; ++l) {
            weights[l] = old_weights[l];
        }
    }

    // Clean up
    for (int i = 0; i < frag_num; ++i) {
        delete[] cache[i];
    }
    delete[] cache;

    for (int i = 0; i < frag_num; ++i) {
        delete[] descriptors[i];
    }
    delete[] descriptors;

    delete[] TPR;
    delete[] TNR;

    delete[] active_train;
    delete[] inactive_train;
    delete[] active_validation;
    delete[] inactive_validation;

    std::cout << header << "\\" << 'n';
    std::cout << weights[0];
    for (int i = 1; i < desc_len; ++i) {
        std::cout << " " << weights[i];
    }
    std::cout << "\\" << 'n' << "train_AUC_no_weights:  " << AUC_const << "  train_AUC_weights:  " << AUC
              << " train_AUC_rand_weights: " << AUC_random << " num_of_descriptors: " << desc_len
              << " num_of_fragments: " << frag_num << "\\" << 'n';
}

// Test the weights
void phase_2(char *argv[], double *weights) {
    double starting_val = 1.0;

    std::string line;
    std::ifstream infile_1(argv[1]);
    std::ifstream infile_2(argv[2]);
    // Method
    line = argv[3];
    std::string annealing_type = argv[4];
    double T = std::stod(argv[5]);
    double orig_T = std::stod(argv[5]);
    double dist;
    int parts = std::stoi(argv[7]);
    double cooling = std::stod(argv[8]);
    std::string phase = argv[9];
    std::ifstream weights_file(argv[10]);

    Comparer *comparer;
    int method = 0;

    if (line == ("simple")) {
        comparer = new Simple_NFrag();
        method = -1;
    } else {
        if (line[0] == 's') {
            std::string number = line.substr(6, line.size() - 1);
            method = std::stoi(number);
            comparer = new Simple_NFrag();
        }
    }

    if (line == ("euclidean")) {
        comparer = new Euclidean_NFrag();
        method = -1;
    } else {
        if (line[0] == 'e') {
            std::string number = line.substr(9, line.size() - 1);
            method = std::stoi(number);
            comparer = new Euclidean_NFrag();
        }
    }

    if (line == ("manhattan")) {
        comparer = new Manhattan_NFrag();
        method = -1;
    } else {
        if (line[0] == 'm') {
            std::string number = line.substr(9, line.size() - 1);
            method = std::stoi(number);
            comparer = new Manhattan_NFrag();
        }
    }

    std::vector<val> best_compared;
    int list_size = 0;
    std::vector<int> max_frag_nums;
    val **cache;
    double **descriptors;
    std::string header;

    std::getline(infile_2, line);
    int desc_len = std::stoi(line);

    std::getline(infile_2, line);
    int frag_num = std::stoi(line);

    std::getline(infile_2, line);
    int active_num_train = std::stoi(line);

    std::getline(infile_2, line);
    int active_num_validation = std::stoi(line);

    std::getline(infile_2, line);
    int active_num_test = std::stoi(line);

    std::getline(infile_2, line);
    int inactive_num_train = std::stoi(line);

    std::getline(infile_2, line);
    int inactive_num_validation = std::stoi(line);

    std::getline(infile_2, line);
    int inactive_num_test = std::stoi(line);

    dist = ((starting_val / desc_len) * std::stod(argv[6])) / 100;

    // Merge train and validation molecules together
    active_num_train += active_num_validation;

    // Array if vectors
    std::vector<int> *active_train = new std::vector<int>[active_num_train];
    std::vector<int> *inactive_train = new std::vector<int>[inactive_num_train];
    std::vector<int> *active_validation = new std::vector<int>[active_num_validation];
    std::vector<int> *inactive_validation = new std::vector<int>[inactive_num_validation];
    std::vector<int> *active_test = new std::vector<int>[active_num_test];
    std::vector<int> *inactive_test = new std::vector<int>[inactive_num_test];

    cache = new val *[frag_num];
    for (int i = 0; i < frag_num; i++)
        cache[i] = new val[frag_num];

    // Clear cache just to be sure
    for (int i = 0; i < frag_num; ++i) {
        for (int j = 0; j < frag_num; ++j) {
            cache[i][j].value = 0;
            cache[i][j].declared = false;
        }
    }

    //float descriptors[frag_num][desc_len];
    descriptors = new double *[frag_num];
    for (int i = 0; i < frag_num; i++)
        descriptors[i] = new double[desc_len];

    // Load fragments descriptors
    int desc = 0;
    std::string token;
    for (int i = 0; i < frag_num; i += 1) {
        std::getline(infile_2, line);
        std::istringstream buf(line);
        while (buf >> token) {
            descriptors[i][desc] = std::stof(token);
            desc++;
        }
        desc = 0;
    }

    max_frag_nums.clear();
    max_frag_nums.push_back(fill(active_train, active_num_train, infile_2));
    // We can't read  active validation molecules, since we merged them with active train
    //max_frag_nums.push_back(fill(active_validation, active_num_validation, infile_2));
    max_frag_nums.push_back(fill(active_test, active_num_test, infile_2));
    // Just to skip the inactive train molecules
    fill(inactive_train, inactive_num_train, infile_2);
    // Just to skip the inactive validation molecules
    fill(inactive_validation, inactive_num_validation, infile_2);
    max_frag_nums.push_back(fill(inactive_test, inactive_num_test, infile_2));

    delete[] inactive_train;
    delete[] inactive_validation;

    list_size = *std::max_element(max_frag_nums.begin(), max_frag_nums.end());
    list_size++;

    double *TPR_test = new double[active_num_train * active_num_test + active_num_train * inactive_num_test];
    double *TNR_test = new double[active_num_train * active_num_test + active_num_train * inactive_num_test];

    best_compared.clear();
    best_compared.reserve(active_num_train * active_num_test + active_num_train * inactive_num_test);

    Node *linked_list_2 = new Node[list_size];


    double AUC = 0;
    double AUC_const = 0;
    double AUC_random = 0;

    if (parts > desc_len) {
        parts = desc_len;
    }

    // Calculate AUC with weights
    // Load weights from file if not already created (phase=="both")
    if (phase == "test") {
        weights = new double[desc_len];
        std::getline(weights_file, line);
        std::getline(weights_file, line);
        std::istringstream buf(line);
        desc = 0;
        while (buf >> token) {
            weights[desc] = std::stod(token);
            desc++;
        }
    }

    AUC = iteration(active_train, active_test, inactive_test, active_num_train, active_num_test, inactive_num_test,
                    cache, descriptors, desc_len, best_compared, method, weights, linked_list_2, TPR_test, TNR_test,
                    comparer, frag_num);

    //Random weights
    for (int k = 0; k < desc_len; ++k) {
        weights[k] = rand_double(0, starting_val / desc_len);
    }
    AUC_random = iteration(active_train, active_test, inactive_test, active_num_train, active_num_test,
                           inactive_num_test, cache, descriptors, desc_len, best_compared, method, weights,
                           linked_list_2, TPR_test, TNR_test, comparer, frag_num);

    //Constant weights
    for (int k = 0; k < desc_len; ++k) {
        weights[k] = starting_val / desc_len / 2;
    }
    AUC_const = iteration(active_train, active_test, inactive_test, active_num_train, active_num_test,
                          inactive_num_test, cache, descriptors, desc_len, best_compared, method, weights,
                          linked_list_2, TPR_test, TNR_test, comparer, frag_num);


    std::cout << "test_AUC_no_weights:  " << AUC_const << "  test_AUC_weights:  " << AUC << " test_AUC_rand_weights: "
              << AUC_random << " num_of_descriptors: " << desc_len << " num_of_fragments: " << frag_num << "\\" << 'n';
}


int main(int argc, char *argv[]) {

    std::string seed = argv[11];

    if (seed != "RANDOM") {
        srand(std::stof(seed));
    } else {
        srand(time(NULL));
    }

    double *weights;
    std::string phase = argv[9];

    if (phase == "model" || phase == "both") {

        std::string line;
        std::ifstream infile_1(argv[1]);
        std::getline(infile_1, line);
        int desc_len = std::stoi(line);

        weights = new double[desc_len];

        phase_1(argv, weights);
    }

    if (phase == "test" || phase == "both") {
        phase_2(argv, weights);
    }

    return 0;
}
