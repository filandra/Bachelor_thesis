
#ifndef SCREENING_EXE_VAL_H
#define SCREENING_EXE_VAL_H


// Small struct which is used in matrix for caching

struct val {
    bool declared;
    double value;

    val(bool dec, float valu) {
        declared = dec;
        value = valu;
    }

    val() {
        declared = false;
        value = 0;
    }
};

#endif //SCREENING_EXE_VAL_H
