

#include "Manhattan_NFrag.h"
#include <cmath>
#include <algorithm>
#include <iostream>

/*
Calculation of Manhattan similarity and Nfrag Manhattan similarity
*/

void
Manhattan_NFrag::compare_sets(std::vector<int> *first_set, int first_num, std::vector<int> *second_set, int second_num,
                              val **cache, double **descriptors, int desc_len, std::vector<val> &best_compared,
                              int method, bool activity, double *weights, Node *linked_list, std::string phase) {
    float result = 0;
    float min_distance = 1000;

    // Compares molecule from second set with each molecule o first set. Best comparison is saved.
    for (int j = 0; j < second_num; ++j) {
        for (int i = 0; i < first_num; ++i) {
            result = mols_distance(first_set[i], second_set[j], cache, descriptors, desc_len, method, weights,
                                   linked_list);
            if (result < min_distance) {
                min_distance = result;
            }
        }
        best_compared.push_back(val(activity, min_distance));
        min_distance = 1000;
    }
};


float Manhattan_NFrag::mols_distance(std::vector<int> &first_mol, std::vector<int> &second_mol, val **cache,
                                     double **descriptors, int desc_len, float method, double *weights,
                                     Node *linked_list) {
    double total = 0;
    int first_frag = 0;
    int second_frag = 0;
    int N = 0;

    if (method == -1) {
        N = first_mol.size() * second_mol.size();
        std::vector<double> compared_values;
        compared_values.reserve(N);

        for (int i = 0; i < first_mol.size(); ++i) {
            first_frag = first_mol[i];
            for (int j = 0; j < second_mol.size(); ++j) {
                //first_mol[i] is a give fragment form a molecule
                second_frag = second_mol[j];
                if (cache[first_frag][second_frag].declared == true) {
                    //Insert
                    compared_values.push_back(cache[first_frag][second_frag].value);
                } else {
                    for (int k = 0; k < desc_len; ++k) {
                        if (isnan((descriptors[first_frag][k] - descriptors[second_frag][k]))) {
                            total += 0;
                        } else {
                            total += std::abs(descriptors[first_frag][k] - descriptors[second_frag][k]) * weights[k];
                        }
                    }
                    cache[first_frag][second_frag].declared = true;
                    cache[first_frag][second_frag].value = total;
                    cache[second_frag][first_frag].declared = true;
                    cache[second_frag][first_frag].value = total;
                    //Insert
                    compared_values.push_back(total);
                    total = 0;
                }
            }
        }
        total = 0;
        //Average
        for (int l = 0; l < N; ++l) {
            total += compared_values[l];
        }
        total = total / (N * desc_len);
        return total;

    } else {
        N = std::ceil(std::max(first_mol.size(), second_mol.size()) * (method / 100));
    }

    if (N == 0) {
        return 0;
    }


    linked_list[0] = Node(0);
    linked_list[N] = Node(N);
    for (int l = 1; l < N; ++l) {
        linked_list[l] = Node(l);
        linked_list[l - 1].next = &linked_list[l];
    }
    int first = 0;
    Node *position;
    int juggle = N;

    double num;
    bool flag;
    for (int i = 0; i < first_mol.size(); ++i) {
        first_frag = first_mol[i];
        for (int j = 0; j < second_mol.size(); ++j) {
            //first_mol[i] is a give fragment form a molecule
            second_frag = second_mol[j];
            if (cache[first_frag][second_frag].declared == true) {
                //Sort insert if value is cached
                num = cache[first_frag][second_frag].value;
                if (num < linked_list[first].value) {
                    flag = false;
                    position = &linked_list[first];
                    while (position->next != NULL) {
                        if (position->next->value < num) {
                            linked_list[juggle].value = num;
                            linked_list[juggle].next = position->next;
                            position->next = &linked_list[juggle];
                            flag = true;
                            break;
                        }
                        position = position->next;
                    }
                    if (flag == false) {
                        linked_list[juggle].value = num;
                        linked_list[juggle].next = NULL;
                        position->next = &linked_list[juggle];
                    }
                    juggle = first;
                    first = linked_list[first].next->position;
                }
            } else {
                // Calculate distance if value is not cached
                for (int k = 0; k < desc_len; ++k) {
                    if (isnan((descriptors[first_frag][k] - descriptors[second_frag][k]))) {
                        total += 0;
                    } else {
                        total += std::abs(descriptors[first_frag][k] - descriptors[second_frag][k]) * weights[k];
                    }
                }
                cache[first_frag][second_frag].declared = true;
                cache[first_frag][second_frag].value = total;
                cache[second_frag][first_frag].declared = true;
                cache[second_frag][first_frag].value = total;
                //Sort insert
                if (total < linked_list[first].value) {
                    flag = false;
                    position = &linked_list[first];
                    while (position->next != NULL) {
                        if (position->next->value < total) {
                            linked_list[juggle].value = total;
                            linked_list[juggle].next = position->next;
                            position->next = &linked_list[juggle];
                            flag = true;
                            break;
                        }
                        position = position->next;
                    }
                    if (flag == false) {
                        linked_list[juggle].value = total;
                        linked_list[juggle].next = NULL;
                        position->next = &linked_list[juggle];
                    }
                    juggle = first;
                    first = linked_list[first].next->position;
                }

                total = 0;
            }
        }
    }

    position = &linked_list[first];
    while (position != NULL) {
        total += position->value;
        position = position->next;
    }

    total = total / (N * desc_len);

    return total;
}