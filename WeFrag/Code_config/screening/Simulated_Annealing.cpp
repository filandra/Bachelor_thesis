//
// Created by adam on 11.7.17.
//

#include "Simulated_Annealing.h"
#include <algorithm>

/*
Calculation weights using simulated annealing
*/

void Simulated_Annealing::calc_weights(double *old_weights, double *weights, double &old_AUC, double new_AUC, double &T,
                                       double dist, int parts, int desc_len, double &cooling, double orig_T,
                                       double starting_val) {
    int position_positive = 0;
    int position_negative = 0;

    double shrink = 1 - (0.3 * (1 - ((T - 0.1) / (orig_T - 0.1))));
    double random = (double) rand() / RAND_MAX;
    double delta = new_AUC - old_AUC;
    double chance_to_move = 0;
    if (delta > 0) {
        chance_to_move = 1;
    } else {
        chance_to_move = exp((delta * 10000) / T);
    }

    if (chance_to_move > random) {
        //Move to new point
        for (int i = 0; i < desc_len; ++i) {
            old_weights[i] = weights[i];
        }
        old_AUC = new_AUC;
    } else {
        //Stay in old point
        for (int i = 0; i < desc_len; ++i) {
            weights[i] = old_weights[i];
        }
    }

    //Shrinking doesn't help
    shrink = 1;

    //TODO dát sem checker
    //Change vector
    for (int j = 0; j < parts; ++j) {
        position_positive = rand() % desc_len;
        position_negative = rand() % desc_len;

        /*if(weights[position_positive]+dist*shrink>starting_val || weights[position_negative]-dist*shrink<0){
            continue;
        }*/
        if (rand() % 2 == 0) {
            if (weights[position_positive] + dist * shrink <= starting_val / desc_len) {
                weights[position_positive] += dist * shrink;
            } else {
                weights[position_positive] = starting_val / desc_len;
            }
        } else {
            if (weights[position_negative] - dist * shrink >= 0) {
                weights[position_negative] -= dist * shrink;
            } else {
                weights[position_negative] = 0;
            }
        }
    }

    T = T * cooling;
};