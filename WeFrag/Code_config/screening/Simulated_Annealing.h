//
// Created by adam on 11.7.17.
//

#ifndef SCREENING_SIMULATED_ANNEALING_H
#define SCREENING_SIMULATED_ANNEALING_H

#include "Optimizer.h"

/*
Calculation weights using simulated annealing
*/


class Simulated_Annealing : public Optimizer {
public:
    virtual void
    calc_weights(double *old_weights, double *weights, double &old_AUC, double new_AUC, double &T, double dist,
                 int parts, int desc_len, double &cooling, double orig_T, double starting_val);
};


#endif //SCREENING_SIMULATED_ANNEALING_H
