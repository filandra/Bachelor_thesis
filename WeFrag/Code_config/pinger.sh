#!/usr/bin/env bash

# Just a small bash script for communication through sshpass and the use of ping utility.

name=$1     # name of computer which we want to adress
command=$2  # what to do [online, activity, start]
login=$3    # login to use in sshpass
pass=$4     # password to use in sshpass
dir=$5      # directory in which the server application resides on server

cpu_limit=60
mem_limit=15

if [ "$command" = "online" ]; then
    sleep 0.1
    ping -c 1 "$name" >> /dev/null
    result=$?
    if [ "$result" = "0" ]; then
        echo "alive"
    else
        sleep 0.1
        ping -c 1 "$name" >> /dev/null
        result=$?
        if [ "$result" = "0" ]; then
            echo "alive"
        else
        echo "dead"
        fi
    fi
fi

# if connection is not possible, result=1, x is empty, stdout and stderr contains "error"
# if connected and grep returns something, result=0
# if connected and grep returns nothing, result=1

if [ "$command" = "activity" ]; then
    sleep 0.1
    x=`sshpass -p "$pass" ssh -o StrictHostKeyChecking=no "$login@$name" "ps aux | less" | grep -vw -E "root|at|message\+|nobody|polkitd|%CPU|sshd|xdm|$login"`
    result=$?
    if [ "$result" -ne "1" ]; then
        sleep 0.1
        x=`sshpass -p "$pass" ssh -o StrictHostKeyChecking=no "$login@$name" "ps aux | less" | grep -vw -E "root|at|message\+|nobody|polkitd|%CPU|sshd|xdm|$login"`
        result=$?
        if [ "$result" -ne "1" ]; then
            cpu=`echo "$x" | awk '{total = total + $3}END{print total}'`
            mem=`echo "$x" | awk '{total = total + $4}END{print total}'`

            awk -v n1=$cpu -v n2=$cpu_limit -v n3=$mem -v n4=$mem_limit 'BEGIN {if (n1<=n2 && n3<=n4) print ("free"); else print ("taken");}'
	    else
	    echo "free"
	    fi
	else
	echo "free"
	fi
fi

if [ "$command" = "start" ]; then
        sleep 0.1
        sshpass -p "$pass" ssh -o StrictHostKeyChecking=no "$login@$name" "bash $dir/starter.sh $dir" >> /dev/null
    result=$?
    if [ "$result" -ne "0" ]; then
        sleep 0.1
	    sshpass -p "$pass" ssh -o StrictHostKeyChecking=no "$login@$name" "bash $dir/starter.sh $dir" >> /dev/null
	    result=$?
	    if [ "$result" -ne "0" ]; then
          echo "dead"
        else
        echo "started"
	    fi
	else
	echo "started"
	fi
fi