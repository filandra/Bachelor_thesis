#!/usr/bin/env bash

# In this script user can choose what commands to run before the start of a server. In this example we activate the RDKit on server computer.

dir=$1  # directory where the server application resides on server
export PATH=~/anaconda3/bin:$PATH
source activate my-rdkit-env
nice -n 19 python3 "$dir/WeFrag_server.py" >> /dev/null &