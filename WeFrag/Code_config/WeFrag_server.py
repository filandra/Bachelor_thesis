#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Simple server side of our program. Receives commands, creates new thread, runs the commands as subprocess, sends back resutls.
"""

import threading
import socket
from subprocess import Popen, PIPE, TimeoutExpired
import math


# Class holding information and functions about given socket connection
class Server_Socket():
	def __init__(self):
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.bind((socket.gethostname(), port))
		self.socket.listen(socket.SOMAXCONN)

	def send(self, script_param):
		self.socket.sendall(script_param)

	def receive(self):
		client, addr = self.socket.accept()
		data = client.recv(100000).decode()
		return client, data

	def close(self):
		self.socket.close()


port = 1984


# This is where the parameters which were send from client are run and where the results are collected
def run_subprocess(client_data):
	args = client_data[1]
	# If computation successful sends the result back, if not, sends error message and closes the connection
	process = Popen(args, stdout=PIPE, stderr=PIPE, shell=True)
	try:
		stdout, stderr = process.communicate(timeout=limit)
		client_data[0].send(stdout)
		client_data[0].close()
	except TimeoutExpired:
		process.kill()
		client_data[0].send(("Timeout error").encode())
		client_data[0].close()


def create_thread(function, params):
	thread = threading.Thread(target=function, args=params)
	thread.daemon = True
	thread.start()


def _main():
	server = Server_Socket()
	message = ""
	# Emergency killer message, however we do not use it (we kill everything on the server instead)
	while not message == "RIP":
		# creates new thread for new connection - client sends parameters for computation (whole bash commands)
		client_data = server.receive()
		create_thread(run_subprocess, [client_data])
		message = client_data[1]
	server.close()
	exit()


limit = 5000
if __name__ == '__main__':
	_main()
