# !/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

"""
Just a simple script which removes duplicate molecules in .sdf file. Used to reduce the size of .sdf files.

-i - input .sdf file
-o - output .sdf file
"""


def _read_configuration():
	parser = argparse.ArgumentParser(
		description='.sdf files merger')
	parser.add_argument('-i', type=str, dest='input',
						help='Input .sdf',
						required=True)
	parser.add_argument('-o', type=str, dest='output',
						help='Output .sdf',
						required=True)

	return vars(parser.parse_args())


def _main():
	configuration = _read_configuration();

	names_found = []
	input_name = True
	new_name = False

	with open(configuration["output"], "w+") as o:
		with open(configuration["input"]) as f:
			for line in f:
				if input_name == True:
					name = line.strip()
					if name not in names_found:
						new_name = True
						names_found.append(name)
					else:
						new_name = False
					input_name = False

				if new_name == True:
					o.write(line)

				if line.strip() == "$$$$":
					input_name = True


if __name__ == '__main__':
	_main()
