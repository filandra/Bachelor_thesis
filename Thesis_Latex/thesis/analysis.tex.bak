\chapter{WeFrag Analysis}
\label{Analysis}

The project is implemented using mix of Python, C++ and Bash. Python is used to implement the main logic and functionality including client-server communication. C++ is used to handle the most computationaly demanding part - simulated annealing and screening. Bash is used for few simple scripts. C++ program is called \textit{screening} and can be used independently on on the python code.

\smallskip
We choose Python because of RDKit, which is library used to extract fragments and assign descriptors (and has many other useful functions like AUC calculation), is implemented in Python. The original plan was to use Python on screening and annealing too, but in the end we switched to C++. Hence we never actualy use RDKit in our code, but Python was still a great choice, because the general logic of a program is written much faster in this langauge.

\smallskip
C++ was chosen for its performance and also because it does not require any virtual machine like Java or C\# does. Thus our cluster does not need to have .Net, Mono or JVM installed.

\smallskip
Python code consists of a set of scripts:
\begin{itemize}[noitemsep]
\item \textbf{main.py} - Handles communication with user, communication with servers, manages given tasks (their assignment to servers or local computer)
\item \emph{WeFrag\textunderscore server.py} - Communicates with client, runs computations on server.
\item \textbf{merge\textunderscore sdf.py} - Small script which merges sdf files provided.
\item \textbf{remove\textunderscore duplicates.py} - Small script which removes duplicate molecules from .sdf file.
\item \textbf{split\textunderscore info.py} - Generates info about split from given .sdf files into a .json file.
\item \textbf{extract\textunderscore fragments.py} - Creates .json file containing information about molecules and its fragments. This script was provided by Petr Škoda.
\item \textbf{padel\textunderscore descriptors.py} - Assignes PaDEL descriptors to each fragment. Requires .json file of molecules and fragments (same file which extract\textunderscore fragments.py outputs). This script was provided by Petr Škoda.
\item \textbf{rdkit\textunderscore descriptos.py} - Assignes RDKit descriptors to each fragment. Requires .json file of molecules and fragments (same file which extract\textunderscore fragments.py outputs). This script was provided by Petr Škoda.
\item \textbf{phase\textunderscore preprocess.py} - Final preprocess of training or test data. For train data emoves correlated descriptors, fragment noise and scales the descriptors. For test data removes descriptors descriptors not used in weights, fragment noise and scales the descriptors.
\end{itemize}

\smallskip
Bash scripts consist of:
\begin{itemize}[noitemsep]
\item \textbf{generate\textunderscore data.sh} - Generates data in a format useful for screening from the input .sdf files. Executes the scripts used for preprocessing of data and checks for existing/non-existing files. Used with the directory system. Used when commands are provided through menu.
\item \textbf{cmd\textunderscore line\textunderscore run.sh} Does the same as generate\textunderscore data.sh but do not relie on directory system and on top of that runs the screening. Used when commands are provided through command line.
\item \textbf{pinger.sh} - Can check if cluster computer is online, if someone is using it or to start WeFrag\textunderscore server.py on it.
\item \textbf{starter.sh} - Small script which is meant to inicialise supporting software before the start of server. We are using to start RDKit, it can be rewritten for other needs.
\end{itemize}

\smallskip
C++ program \textit{screening} consist of:
\begin{itemize}[noitemsep]
\item \textbf{screening.cpp} - Handles main logic, reading input and producing output.
\item \textbf{Comparer.cpp} and \textbf{Comparer.h} - Parent class used in order to easily implement new similarity functions for comparing molecules.
\item \textbf{Euclidean\textunderscore NFrag.cpp} and \textbf{Euclidean\textunderscore NFrag.h} - Child of Comparer, implements euclidean similarity and Nfrag euclidean similarity.
\item \textbf{Manhattan\textunderscore NFrag.cpp} and \textbf{Manhattan\textunderscore NFrag.h} - Child of Comparer, implements manhattan similarity and Nfrag manhattan similarity.
\item \textbf{Simple\textunderscore NFrag.cpp} and \textbf{Simple\textunderscore NFrag.h} - Child of Comparer, implements simple similarity and Nfrag simple similarity.
\item \textbf{Node.h} and \textbf{val.h} - Only supporting struct.
\item \textbf{Optimizer.cpp} and \textbf{Optimizer.h} - Parent class used in order to easily implement new optimizing functions for finding weights.
\item \textbf{Simulated\textunderscore Annealing.cpp} and \textbf{Simulated\textunderscore Annealing.h} - Child of Optimizer, implements simulated annealing.
\end{itemize}

\section{Client-Server Communication}

\smallskip
This whole funcionality has been designed to work mainly on the computer laboratory of Faculty of Mathematics and Physics, Charles University. In theory it should work with any cluster of computers with shared file system.

\smallskip
Users can configure which computers to use as a part of their cluster in cluster\textunderscore config.txt.

\smallskip
When talking about our cluster computing system, we need to first understand what a \textit{task} is. We define task as a screening of a split or preprocessing of certain configuration. For example when user wants to perform screening of 10 splits with certain configuratin, 11 tasks are created. First the preprocessing task is executed on the cluster and then the 10 screening tasks are performed in paralel. All screening tasks can be computed in paralel, hovewer some preprocessing tasks cannot be performed in paralel and they always need to be computed before screening tasks. This is why cluster is so useful - with cca. 38 computers in our laboratory we can in theory preprocess 38 different configurations in paralel - and mind that some configurations partialy use data from other configurations so this speeds up the preprocessing even more when more configurations are computed. After all preprocessing is finished we can in theory compute 304 splits in paralel (8 tasks per computer is the maximum number).

\smallskip
In case the user does not want to send data (which can be farily big) to the cluster, he/she can preprocess the data localy and then send only the preprocessed data used for screening to the cluster.

\smallskip
The server application on cluster servers is started through SSH. After that the cluster is controlled through python sockets and data is sent to computers through SCP. In order not to disturbe user by repeatedly asking him for password when starting the cluster (since we need ssh command for every computer in cluster), we use sshpass. Hovewer there is a security risk involved in this solution, since the sshpass command can be found in command history containing uncyphered password and login. This could be solved by enabling ssh keys, hovewer it is another level of dificulty for new user to start using cluster. If the user is running WeFrag on his secured computer, sshpass should not be a security issue.

\smallskip
When starting the cluster we scan computers and those which are dead or occupied by someone else (and use significant ammount of resources) are excluded. This scaning can be periodically repeated in order to avoid blocking any computer. The whole system is also sturdy against sudden death of a computer (tasks computed on this computer will be redirected somewhere else). The user can also configure how many tasks to compute on a computer.
 
\smallskip
The core of WeFrag\textunderscore server.py is the use of sockets, threading and bash subprocess, which the python language enables. The whole process looks like this:
\begin{itemize}[noitemsep]
\item Task is chosen from queue of tasks
\item Server with free computation capabilites is selected
\item Bash command of task in form of string is generated on client
\item Socket connection to the server is established
\item Server creates new thread for this connection
\item String with command is sent to the server
\item Server receives the command and runs it as a subprocess
\item Server collects the result of the subprocess
\item Server sends the result back to the client
\item Client closes the socket connection and handles the response
\end{itemize}

Failur in any of the steps above should only result in putting the task to the back of the queue. 