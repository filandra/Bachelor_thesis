\begin{thebibliography}{10}

\bibitem{TodeschiniCnsonni}
Roberto Todeschini and Viviana Consonni.
\newblock {\em Handbook of Molecular Descriptors}.
\newblock Wiley-VCH, 2000.

\bibitem{HokszaSkoda}
Škoda~P. Hoksza~D.
\newblock Using bayesian modeling on molecular fragments features for virtual
  screening.
\newblock {\em IEEE/ACM Transactions on Computational Biology and
  Bioinformatics}, 2016.

\bibitem{LipinskiHopkins}
C.~Lipinski and A.~Hopkins.
\newblock Navigating chemical space for biology and medicine.
\newblock {\em Nature}, page 855–861, 2004.

\bibitem{Bohacek}
McMartin~C. Bohacek, R.S. and W.C. Guida.
\newblock The art and practice of structure-based drug design: a molecular
  modeling perspective.
\newblock {\em Medicinal Research Reviews}, page 3–50, 1996.

\bibitem{VS}
Hugo Kubinyi Gerd~Folkers Christoph~Sotriffer, Raimund~Mannhold.
\newblock {\em Virtual Screening: Principles, Challenges, and Practical
  Guidelines}.
\newblock Wiley-VCH, 2011.

\bibitem{Maggiora}
M.A. Johnson and G.M. Maggiora.
\newblock {\em Concepts and Applications of Molecular Similarity}.
\newblock John Wiley \& Sons, Inc., New York, 1990.

\bibitem{Petterson}
Cramer R.D. Ferguson A.M. Clark~R.D. Patterson, D.E. and L.E. Weinberger.
\newblock Neighborhood behavior: a useful concept for validation of molecular
  diversity descriptors.
\newblock {\em Journal of Medicinal Chemistry}, page 3049–3059, 1996.

\bibitem{Stahura}
F.L. Stahura and J.~Bajorath.
\newblock Virtual screening methods that complement hts.
\newblock {\em Combinatorial Chemistry \& High Throughput Screening},
  7:259–269, 2004.

\bibitem{Lombardo}
F.; Dominy B.W.; Feeney~P.J. Lipinski, C.A.;~Lombardo.
\newblock Experimental and computational approaches to estimate solubility and
  permeability in drug discovery and development settings.
\newblock {\em Adv. Drug Deliv. Rev.}, pages 3--25, 1997.

\bibitem{Lionta}
Vassilatis DK Cournia~Z Lionta~E, Spyrou~G.
\newblock Structure-based virtual screening for drug discovery: principles,
  applications and recent advances.
\newblock {\em Curr Top Med Chem}, pages 1923--38, 2014.

\bibitem{Todeschini}
V.~Todeschini, R.;~Consonni.
\newblock {\em Molecular Descriptors for Chemoinformatics}.
\newblock Wiley-VCH, 2009.

\bibitem{fingerprint}
Qiannan Hu Yi-Zeng~Liang Dong-Sheng~Cao, Qingsong~Xu.
\newblock Manual for chemopy.
\newblock \url{https://www.researchgate.net}.
\newblock Accessed: 2010-09-30.

\bibitem{PaDEL}
Yap CW.
\newblock Padel-descriptor: An open source software to calculate molecular
  descriptors and fingerprints.
\newblock {\em Journal of Computational Chemistry}, pages 1466--1474, 2011.

\bibitem{RDKit}
Greg Landrum.
\newblock Rdkit: Open-source cheminformatics.
\newblock \url{http://www.rdkit.org}.
\newblock Accessed: 2010-09-30.

\bibitem{CDK}
C.; Kuhn S.; Floris M.; Guha R.; Willighagen E.~L. Steinbeck, C.;~Hoppe.
\newblock Recent developments of the chemistry development kit (cdk) - an
  open-source java library for chemo- and bioinformatics.
\newblock {\em Current Pharmaceutical Design}, pages 2111 -- 2120, 2006.

\bibitem{CDL}
D.~E.~J. Sykora, V. J.;~Leahy.
\newblock Chemical descriptors library (cdl): a generic, open source software
  library for chemical informatics.
\newblock {\em Chem Inf Model}, page 1931, 2008.

\bibitem{TT}
J.~S.~Dixon R.~Nilakantan, N.~Bauman and R.~Venkataraghavan.
\newblock Topological torsion: A new molecular descriptor for sar applications.
  comparison with 0ther descriptors.
\newblock {\em J Chem Inf Comput Sci}, page 82–85, 1987.

\bibitem{ECFP}
D.~Rogers and M.~Hahn.
\newblock Extended-connectivity fingerprints.
\newblock {\em J Chem Inf Comput Sci}, page 742–754, 2010.

\bibitem{ecfpFrag}
ChemAxon.
\newblock Chemaxon docs.
\newblock \url{https://docs.chemaxon.com}.
\newblock Accessed: 2010-09-30.

\bibitem{Annealing}
Jan K.~Lenstra Emile~Aarts.
\newblock {\em Local Search in Combinatorial Optimization}.
\newblock John Wiley \& Sons, 1997.

\bibitem{Bench}
Škoda~P. Hoksza~D.
\newblock Benchmarking platform for ligand-based virtual screening.
\newblock {\em Bioinformatics and Biomedicine (BIBM)}, 2016.

\end{thebibliography}
