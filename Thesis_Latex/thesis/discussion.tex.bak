\chapter{Discussion}

\smallskip
In our work we have implemented weighted fragment-feature molecular representation, designed multiple similarity functions for this representation, implemented simulated annealing and added the elimination of correlated descriptors and fragment noise. We also created a tool for large scale computation of experiments.

\smallskip
We have demonstrated that when given the right similarity function, weights found through simulated annealing improve the performance of weighted fragment-feature molecular representation. We have also found a similarity function which performs almost as well as other traditional methods. The influence of different parameters of our approach was examined through experiments and we determined which are benefitial and which are not. Thus we completed the two main goals of our work.

\smallskip
All of the subgoals of our work were also completed. We found that the best performing fragments are circular fragments of radius 1, best performing descriptors are from RDKit, euclidean100 is the best performing similarity function and we explored the correlation treshold and fragment noise. 

\smallskip
As we were able to both succesfuly design well performing similarity function, demonstrate the potential of weights found through simulated annealing and complete all of the subgoals, we think of this work as a succes.

\section{Future work}

\smallskip
With the information obtained through this work, we see a potential in weighted fragment-feature representation paired with simulated annealing. As the similarity function seems to be the most influential factor, we think that a solution to outperform the traditional methods lies in finding a better one. Perhaps similarity function based on the Nfrag similarity, hovewer altered in such a way that weights improve it.

\smallskip
We also want to use our method on other datasets, such as datasets with very low and very high number of training molecules. Also it would be interesting to try extremely long training times on these datasets.