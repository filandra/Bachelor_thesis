\chapter{WeFrag}


\smallskip
When designing our program WeFrag we focused mainly on the ability to easily configure various aspects of our approach to virtual screening (e.g. type of fragments or type of similarity function) and compute those configurations fast. The reason for this is that we needed to perform many experiments in order to find how much certain options influence the performance and find the best configuration. 

\smallskip
To easily perform great number of different computations WeFrag offers client and server side of application for paralel computing on cluster. The user interface enables the user to quickly assign several thousands of different computations (using grid options) which will be then dynamicaly distributed and computed. Results are saved in client computer and can be aggregated into a .csv file using built-in function. WeFrag also provides user with various features regarding the maintenance of cluster - namely monitoring of running computations and the ability to update data and code on servers.

\smallskip
When distributing computations (tasks) on the cluster, we also monitor current state of every cluster computer (usage of memory and CPU) in order not to disturbe other users of cluster.

\smallskip
WeFrag also provides user with option of local computation, when cluster is not avaliable or user wants to run just a small number of computations. Local computations can be again assigned through menu, but there is also the option to use only command line arguments.

\smallskip
More information about the implementation of WeFrag can be found in section \ref{Analysis}. The whole program is included in attachement.

\section{All options WeFrag provides}

\smallskip
Options regarding input:
\begin{itemize}[noitemsep]
\item Input through menu using cluster.
\item Input through menu using local computer.
\item Input through command line using local computer.
\end{itemize}

\smallskip
Capabilities regarding virtual screening and simulated annealing:
\begin{itemize}[noitemsep]
\item To select targets from data, which the user provides.
\item To select which splits to compute.
\item To choose from linear fragments of length 2, 3 and 4 and from circular fragments of radius 1 and 2.
\item To choose from RDKit or PaDEL descriptors.
\item To set fragment noise reduction treshold percentage.
\item To set descriptor correlation treshold percentage.
\item To select similarity function from simple, Euclidean, Manhattan and Nfrag version of simple, Euclidean and Manhattan.
\item To choose from none simulated annealing or simulated annealing with radnom starting position or simulated annealing with constant starting position.
\item To set starting simulated annealing temperature.
\item To set cooling rate.
\item To set the distance of neighbours in simulated annealing.
\item To choose how many dimensions to change in one step of simulated annealing.
\end{itemize}

\smallskip
Other features regarding mainly the monitoring of computation and updating cluster are:
\begin{itemize}[noitemsep]
\item To get information about running computations and state of cluster. Also the ability to stop running and planed computations.
\item To update code, data and preprocessed data on cluster.
\item To set the number of computations per computer.
\item To set the seed for random number generation.
\end{itemize}

\smallskip
Users are also provided with configuration file in which they can configure:
\begin{itemize}[noitemsep]
\item Location of code and data on server.
\item Number of splits.
\item Seed used in randomizer.
\item Names and adresses of cluster computer.
\end{itemize}

\section{Optimization}

\smallskip
Simulated annealing is quite computationally demanding since it requires to calculate the whole virtual screening in each iteration. Since we need at least several thousand iterations, even a few seconds to calculate a screening is too much. For example 10000 iterations (which is quite low number) and 3 seconds per screening would take about 8 hours to compute. On top of that we need to calculate weights for each split (and in our datasets we have 168 splits as explained in section \ref{Data}). That is of course too much for any practical experiments.

\smallskip
At the beggining, when we used Python to implement screening and did not use any custom data format, one single screening took about 20-40 minutes and the whole computation was serial. At the end one screening takes from 0.1 to 0.5 seconds and screenings of splits are computed in paralel on any number of cluster computers. The times vary depending on types of fragments, descriptors and correlation treshold used. Without this improvement we could never execute our experiments.

\smallskip
Other than screenig the data preprocessing (creating fragments, calculating descriptors, removing correlated descriptors etc.) needs to be optimized too. One such preprocessing can take about 15 minutes. Thanks to reusing some preprocessed data we managed to improve this process too.

\smallskip
We have already discussed means of speeding up the computation on the data level - removing fragment noise and correlated descriptors. However great performance improvements were also made through technical solutions. In this section we discuss the most profound ones.


\subsection{Caching}

Caching of fragment comparisons made probably the greatest improvement in performance. This solution seems obvious however it is not so much from the view of a programmer who is not a chemist and does not know much about molecules and its fragments. The idea goes as follows: When comparing molecules we compare its fragments. These fragments are not unique for each molecule - some fragments occur in many molecules. So when we compare a pair of fragments we save the result somewhere and when we need to compare this same pair of fragments again (when comparing some other pair of molecules) we can just use the already computed value. There are two crucial factors which need to be fulfilled in order for caching to work. 

\smallskip
First, there can not be too many different types of fragments or else we would run out of memory - we need to hold an array (or equivalent data type) with size: number of different fragments in all molecules squared. Second, we need to compare some fragments pairs often enough in order for caching to take effect - if every fragment pair was computed only once, caching would even hurt our method.

\smallskip
Both of those requirements are satisfied (which was a little bit surprising). There are typicaly only thousands or tens of thousands different fragment structures. And the same fragment pairs are compared often enough to speed up computation several fold.

\subsection{C++ vs Python}

At first the core of screening and simulated annealing was written in Python. When rewritten into C++ the same computation was suddenly computed much faster. One reason for the speed up is that along with the C++ code we implemented custom data format. With Python code we tried to use fragments in JSON format and descriptors in text format (direct output of RDKit), both represented in memory as dictionary. These two formats proved to be terribly slow thanks to the need to constantly search through the dictionaries. With C++ our data format is focused on pointers and is explained in \ref{Formats}.

\smallskip
The speed up is also probably due to the fact that C++ can perform some optimization during compilation. We also use tightly packed arrays of pointer (in hopes of being cache friendly) in C++ while in Python we used dictionaries, which are much slower. Python is also dynamicaly typed which can generate many overhead machine instructions.

\subsection{Better sorting algorithm}

\smallskip
Another rather simple idea like caching. When comparing molecules using the Nfrag similarity function one need to sort the results of fragment pairs comparisments and pick the top $N$. However using the standard C++ sorting algorithm is an "over kill". We do not need to have the entire set of results sorted - we just need to know the top N. So by implementing this top N sort using linked list we achieved considerable speed up.


\subsection{Memory optimization}

\smallskip
In theory JSON output of fragmented molecules and output of RDKit or PaDEL is everything needed to perform screening and annealing. Data in this format could be as big as several hundred Mbits. This is unacceptable since we want to run multiple screenings on one computer. We also need to create table for caching which can be quite big when larger fragments are used like ecfp.2 (since more unique fragments are then present). So another priority was to reduce the size of data which we need to hold in memory. This was again done through our custom data format explained in \ref{Formats}

\subsection{Reusing the preprocessed data}

\smallskip
Data input is handled in the preprocessing phase, where we convert the raw molecules in SDF format into our molecular representation, which we later use for computations. For each configuration the preprocessing needs to be done only once. First thing to understand when talking about creating our molecular representation is input data format. Input to our program is in form of .sdf files, which is data format used to describe molecules. SDF (structure-data file) is a chemical-data file format which holds structural information about molecule and wraps other data format - MDL Molfile which holds information about the atoms, bonds, connectivity and coordinates of a molecule. This data format was created by MDL Information Systems.

\smallskip
Files can be provided to WeFrag either through a \emph{directory system} or as a \emph{command line parameter}.

\smallskip
The advantage of using input through directory system is that all of the files created (either final or temporary) can be re-used when preprocessing certain other configurations - for example if the type of fragments stays the same, we do not separate fragments again. Or if we want to compute with different correlation treshold, we need to only recompute final preprocessing part. The whole preprocessing phase is explained in section \ref{Preprocessing}

\smallskip
This is possible thanks to the additional information the directory system provides. When using command line input we can not perform such optimizations since we do not have enough information about the data.

\smallskip
Thus directory system saves u a lot of time when testing great number of slightly different configurations since the preprocessing phase can be fairly time consuming.

\subsection{Parallelization}

\smallskip
We used two levels of parallelization. One level is implemented using Python threads. Threads were used only for enabling the client-server communication and for assignment of computations on a server or local computer.

\smallskip
The other level of parallelization is realized by paralelization of preprocessing and screening of splits. Parallelization of preprocessing is only partial - for example 2 splits of the same target can not be preprocessed at the same time, since creation of temporary files would collide.

\smallskip
All screening tasks can be computed in paralel, however some preprocessing tasks cannot be performed in paralel and they always need to be computed before screening tasks. This is why cluster is so useful - with cca. 38 computers in our laboratory we can in theory preprocess 38 different configurations in paralel - and mind that some configurations partialy use data from other configurations so this speeds up the preprocessing even more when more configurations are computed. After all preprocessing is finished we can in theory compute 304 splits in paralel (8 tasks per computer is most efficient). However when using some larger fragment types (such as ecfp.2) only about 1 or 2 splits per computer is possible thanks to massive memory demand.