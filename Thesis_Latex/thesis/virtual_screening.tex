\chapter{Introduction to virtual screening and related theory}
%Možná přidat sekci o general a sekci o ligand based
Due to the nature of given problem and our solution, it is fitting to introduce reader into virtual screening as a whole and some related theory. In this chapter, we first explore the theory and categories of virtual screening, then we shift focus to molecular descriptors and fragments, take a quick look at one popular way used to compare the performance of various virtual screening methods and at last we explore the simulated annealing.

\section{What is virtual screening and its categories}
The problem of finding a molecule with certain desired effects can be ilustrated by the concept of \textit{chemical space} \ref{chemspace} which is an imaginary space containing all possible chemical compounds and \say{can be viewed as being analogous to the cosmological universe in its vastness, with chemical compounds populating space instead of stars} \citep{LipinskiHopkins}. To imagine the size of chemical space one estimate states that there are more than $10^{63}$ molecules with less than 30 atoms and molecular weight less than 500, which are stable at room temperature and stable against oxygen and water \cite{Bohacek}. Thus virtual screening acts as a telescope of sorts, which enables us to look at different compounds in this chemical universe and predict their effects or even predict the existence of certain compounds. The laboratory testing of those compounds (\textit{high-throughput screening} - HTS) would then be equivalent of flying to those places in a spaceship. 


\begin{figure}[!htbp]
  \caption{Diagram of chemical space - ilustration from \cite{VS}}
  \label{chemspace}
  \centering
    \includegraphics[width=\textwidth]{../img/Chemspace.png}
\end{figure}

\smallskip
Target is a short for \textit{biological target} - any structure in organism (e.g. protein or nucleic acid) to which a molecule can bind, resulting in changes of behavior or function of this structure in given organism. Essentially goal of virtual screening is to find new molecules which can bind to a given target and we call these molecules active in respect to given target. \textit{Decoys} are known inactive molecules and \textit{ligands} are known active molecules (in respect to given target). Set of molecules in which we are searching for new active compounds is called set of candidate molecules.

\smallskip
New active molecules which we found also need to fullfil other requirements in order to be useful. One is often interested in molecules which structurally differ from known active compounds - the motivations vary, but the most common ones are to find molecule stronger in activity, molecule with different side-effects or to generate new intelectual property that is different enough from their competitors. Other requirement for new active molecules is to be \textit{druglike}. Druglikeness determines a set of characteristics necessary for compound to be safe and orally administered (absorption, permeability, metabolic stability, toxicity and others) \cite{VS}. All of those requirements can be adressed by virtual screening too, however in our work we focus only on identification of new active compounds.

\smallskip
Since 1970s virtual screening diversified into many different methods. The two main categories of approaches are \textit{ligand-based} (LBVS) and \textit{structure-based} (SBVS) virtual screening.

\smallskip
Underlying principle of LBVS is \textit{similar property principle} \cite{Maggiora} which states that similar molecules function in similar way. One can quickly find arguments against this principle - small changes in compounds resulting in great changes in biological activity or large structural changes resulting in little to no change in activity, but this principle is generally accepted and it has been statistically demonstrated \cite{Petterson}. However what exactly is a "similarity" between molecules is not defined - the whole meaning changes depending on how we describe molecules and how we compare molecules according to this description. For example, we could describe molecules by their molecular weights and similarity function could be just a difference of these weights. Other approach could describe molecules as set of substructures and similarity could be defined as the number of shared substructures divided by the total number of substructures. Thus LBVS methods are mainly distinguished by the method of describing a molecule and by the method of comparing a molecule using this description. Three main sub-categories of LBVS exist - \textit{similarity searching}, \textit{compound classification} and \textit{compound filtering} \cite{Stahura}.

\smallskip
Similarity searching includes namely our approach, volume/surface matching, substrucutre searching, pharmacophore searching and descriptor methods. Although they all differ in how to describe and compare molecules, the general approach stays the same. With at least one already known active compound (compound classification requires multiple known actives) the screening can begin: Create representation of known actives and candidate molecules, compare candidate molecules against known actives, evaluate the results, select small number of molecules for HTS. In our work we focus on the descriptor method.

\smallskip
The compound classification sub-category includes methods such as clustering/partitioning, mapping and various machine learning methods. However this sub-category is not utilised in our work.

\smallskip
Compound filtering is often used as pre-screening and profiling of large compound libraries. The goal is usually to filter out undesired molecules such as reactive and toxic ones. Filtering can be also used for estimating basic ADME (Absorption, Distribution, Metabolism, Excretion) - which are parameters required for orally avaliable medicine (Rule-Of-Five \cite{Lombardo}). We also did not use this sub-category.

\smallskip
Even though we do not focus on SBVS in our work, quick overview can be useful. The SBVS is based on docking the candidate molecules into a target (e.g. protein). This process involves creation of target model from 3D structural information and selection of binding site. With the model prepared, each compound of library is docked into the target. The priority of candidate molecule is determined by the ability of binding to the target with high affinity, estimated by scoring a function for docking. In the end the molecules that bind the best are selected. As always there are multiple approaches on how to dock and score molecules. This category is still very computationaly demanding - LBVS is much less demanding and also receives greater attention. Interested reader can find more information in publication by Lionta \cite{Lionta}.

\section{Ligand based virtual screening with descriptors}

The most inspiration was drawn from this approach. \say{The molecular descriptor is the final result of a logic and mathematical procedure which transforms chemical information encoded within a symbolic representation of a molecule into a useful number or the result of some standardized experiment.} \cite{TodeschiniCnsonni} In other words descriptors represent features and properties of a molecule in form of a value. They can be than formed into vector of values or \textit{fingerprint} (vector of zeros and ones).

There is a great variety of descriptors and they are mainly categorised by their dimension. 1D descriptors describe molecular properties such as weight or number of carbon atoms. The presence, absence, topology or number of different substructures (\textit{molecular fragments}) are 2D descriptors. 3D descriptors encode shape and functionality \cite{Todeschini}.

\begin{figure}[!htbp]
  \caption{Ilustration of descriptors - from \cite{fingerprint}}
  \label{descriptors}
  \centering
    \includegraphics[width=7cm, height=4.5cm]{../img/descriptors.png}
\end{figure}


Descriptors which are formed into vector of values \ref{descriptors} are generated by software such as PaDEL \cite{PaDEL}, RDKit \cite{RDKit}, JOELib, Chemistry Development Kit (CDK) \cite{CDK} and Chemical Descriptors Library \cite{CDL}. Each part of vector represents value of certain chosen 1D, 2D or 3D descriptor. We use PaDEL and RDKit descriptors in our work.

\begin{figure}[!htbp]
  \caption{Ilustration of fingerprint, colorful bits are set to 1, the rest is set to 0 - from \cite{fingerprint}}
  \label{fingerprints}
  \centering
    \includegraphics[width=7cm, height=4.5cm]{../img/fingerprint.png}
\end{figure}

Fingerprints put simply are bitstrings, where each bit represents presence or abscene of certain molecular feature, substructure, range of values or other feature \ref{fingerprints}. Types of fingerprint descriptors include substructure-keys based (e.g. MACCS), topological torsions based \cite{TT} (e.g. Daylight) and circular based (e.g. ECFP \cite{ECFP}). Fingerprints can also be folded. Fingerprints however are not in the focus of our work.

\smallskip
As said earlier LBVS with descriptors is included in sub-category of LBVS called similarity searching. It also follows the general similarity searching approach \ref{lbvs}:
\begin{enumerate}[noitemsep]
\item Create representation for known actives and candidate molecules = assign descriptors to known actives and candidate molecules
\item Compare candidate molecules against known actives = use appropriate similarity/distance function between known actives and candidate molecules using representation from previous step.
\item evaluate the results = use appropriate scoring function
\item select small number of molecules for HTS
\end{enumerate}

\begin{figure}[!htbp]
  \caption{Diagram of LBVS with descriptors}
  \label{lbvs}
  \centering
    \includegraphics[width=0.6\textwidth]{../img/LBVS.png}
\end{figure}


\section{Molecular Fragments}

\smallskip
Because we use molecular fragments in our approach we will take a quick look at the concept here. Molecular fragment is any form of continuous substructure of given molecule, however, we focus on fragments which follow certain rules - namely \textit{linear fragments} and \textit{circular fragments}.

\smallskip
Linear fragments can be imagined as paths through the molecule of certain lengths where length is the number of atoms in path. For example linear fragments of length 1 are all single atoms in a molecule, linear fragments of length 2 are all atom pairs in a molecule. In our work we use linear fragments of length 2, 3 and 4.

Circular fragments consist of substructures of certain radius around atoms where radius is measured in number of atom bonds \ref{ecfp}. For example circular fragment of radius 0 are all single atoms in a molecule, circular fragments of radius 1 consist of central atom and all of its direct neighbours (other atoms connected by bond with the central atom of fragment). In our work we use circular fragments with radius of 1 and 2.

\begin{figure}[!htbp]
  \caption{Ilustration of circular fragments - from \cite{ecfpFrag}}
  \label{ecfp}
  \centering
    \includegraphics[width=\textwidth]{../img/ecfp_generation.png}
\end{figure}

One can also think about molecular fragment as a connected induced subgraph (following certain rules) of a connected graph (which represents the molecule).

\section{RDKit and PaDEL descriptors}
Both PaDEL \cite{PaDEL} and RDKit \cite{RDKit} are open source software for calculating molecular descriptors which we use in our work.

\smallskip
PaDEL currently calculates 797 desciptors (663 1D and 2D, 134 3D descriptors) and was programmed using Java language. Advantages of PaDEL are that it provides both GUI and command line interface, can work on any platform that supports Java (Windows, Linux, MacOS), supports more than 90 different molecular file formats and the speed of computation is quite good (thanks to the usage of multi-thread computation). The disadvantage of PaDEL is that it does not calculate as many descriptors as other software such as DRAGON, Molconn-Z, MODEL and PreADMET Descriptor.

\smallskip
RDKit has core algorithms written in C++ and wrappers in Python, Java and C\#. RDKit has more general purpose than PaDEL, because it can provide other functions besides just descriptor calculation (e.g. molecular fragment extraction). Advantages of RDKit include the support of many input and output formats and ability to work on all major platforms (Windows, Linux, MacOs). However RDKit calculates only 192 descriptors which is even less than PaDEL.

\smallskip
RDKit and PaDEL assign descriptors to any molecular structure, meaning we can assign descriptors either to complete molecules or to molecular fragments.

\section{Scoring - ROC and AUC}
With such a great number of different LBVS methods the question arises, how to compare performance of those methods. One popular way is to calculate area under the curve (AUC) of receiver operating characteristic (ROC) which gives us single number evaluating used method.

\smallskip
ROC curves are used to identify the diagnostic ability of a binary classifier system. The classifier system receives different positive or negative test examples and assignes them positivity or negativity using some algorithm. In the end we can determine how often the system is \textit{true positive} ($TP$ - predicts positivity in positive example), \textit{true negative} ($TN$ predicts negativity in negative example), \textit{false positive} ($FP$ predicts positivity in negative example) and \textit{false negative} ($FN$ predicts negativity in positive example). The curve is created by plotting the \textit{true positive rate} against the \textit{false positive rate}. True postitive rate (sometimes called sensitivity) is defined as ${TP}\over{TP+FN}$ and the false positive rate (sometimes called specifity) is defined as ${TN}\over{TN+FP}$

In other words, ROC curve tells us how well can the algorithm of our classifier  separate active molecules from the inactive ones. To be able to do that, our method (classifier) needs to be ranking - molecules with higher rank have to be more likely to be active. In our approach we rank molecules by the similarity to an already known active molecule.

The algorithm to plot a ROC curve goes as follows:
\begin{itemize}[noitemsep]
\item Rank each molecule and sort them in decreasing order
\item Start at $s=(x,y)$ where $x=0$ and $y=0$
\item For each molecule $m$ (using the decreasing order)

\quad If $m$ is active $x+=1$

\quad If $m$ is inactive $y+=1$

\quad Plot coordinate $s$
\end{itemize}

On this graph the $y$-axis represents the true positive rate and the $x$-axis represents the false positive rate.


\smallskip
AUC (sometimes also AUROC) is used to convert the ROC curve into a number. By computing the area under the ROC curve we give the curve a score. The bigger the score is the better. Perfect AUC equals to 1 - all active molecules are ranked before any negative one. The worst AUC equals to 0 - all inactive molecules are ranked before any active one.

\smallskip
The AUC value is popular way to measure the performance of different virtual screening methods because the only requirement is to have a ranking classifier. One feature of AUC which reader could notice is that only the order of test examples (molecules in our case) is important, not the exact value of classifier. The disadvantage of this feature is that if for example half of all molecules were 10x more similar than the other half and contained all of the active molecules, we would not notice, even though this is probably useful information.

\section{Simmulated Annealing}
\label{AnnTheory}

Simmulated annealing is a method used to find global optimum of a function. We use this method to find weights of the molecular descriptors. In order to explain how simulated annealing works, lets compare it to hillclimbing. The algorithm for hillclimbing (when searching for maxima) is following:

\smallskip
Lets define:

$f: R^{n} -> R$ function for which we want to find global maxima. 
$x, y \in R^{n}$ vectors.
$N: R^{n} -> R^{n}$ neighbour function.
\begin{enumerate}[noitemsep]
\item $x= some \textunderscore initial \textunderscore position$
\item $y=N(x)$
\item If $f(y) > f(x)$ then $x=y$
\item Go to step 2.
\end{enumerate} 

\smallskip
This algorithm can work nicely, if our function has no local maxima. However if there are local maxima, the chance is, we will get stuck in those (and our function has probably many of those). In order to avoid this problem one would like to somehow jump out of the local maxima but in the end stay in the global maxima. 

\smallskip
Which is where simulated annealing comes in. Instead of always moving to better neighbour position, we also allow to sometimes move into worse neighbour position - we define the \textit{acceptance criterion}. The crucial part is, that the acceptance criterion changes in time. This is called the \textit{cooling schedule} and the variable that affects the acceptance criterion is called $temperature$. A cooling chedule is specified by the initial temperature, decrement function for lowering the value of temperature and final value of temperature at which the annealing stops. \cite{Annealing} At first with high temperature we want to move into worse positions relatively frequently but as time goes on and temperature cools down we will behave more and more like hillclimbing. 

\smallskip
The idea is that we jump out of local maxima at the begining and towards the end we climb the global maxima (or value close to global maxima). This concept is inspired by annealing in metalurgy, where controlled cooling yields metals with different crystal structures (and different desired properties). The algorithm for simulated annealing goes as follows:


\smallskip
Lets define:

$f: R^{n} -> R$ function for which we want to find global maxima. 
$x, y \in R^{n}$ vectors
$N: R^{n} -> R^{n}$ neighbour function
$P: (R,R,R) -> <0,1>$ acceptance criterion
$T$ temperature
$C$ decrement function
\begin{enumerate}[noitemsep]
\item $x= some \textunderscore initial \textunderscore position$
\item $y=N(x)$
\item $x=y$ with the probability of $P(f(x), f(y), T))$
\item $T=C(T)$,  go to step 2.
\end{enumerate} 

When implementing this algorithm we need to solve mainly three different issues which are specific for the kind of function for which we want to find global optima: how to implement neighbour positions function, acceptance criterion and decrement funtion. All of those questions are answered in chapter \ref{Annealing}.